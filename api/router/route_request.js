function notFound() {
  return Promise.reject({ usecase: 'NOT_FOUND' });
}

module.exports = function routeRequest(req, res) {
  var payload, usecase;

  switch (req.url) {
    case '/createSession':
      usecase = require('../app/usecases/create_session.js');
      break;
    case '/addTask':
      usecase = require('../app/usecases/add_task.js');
      break;
    default:
      usecase = notFound;
  }

  req.body = [];
  req.on('data', function (part) {
    req.body.push(part.toString());
  });
  req.on('end', function () {
    payload = JSON.parse(req.body.join(''));
    usecase(payload)
      .then(function (result) {
        res.writeHead(200, {});
        res.end(JSON.stringify(result));
      })
      .catch(function (errors) {
        res.writeHead(400, {});
        res.end(JSON.stringify(errors));
      })
  });

};
