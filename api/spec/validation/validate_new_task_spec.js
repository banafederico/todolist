describe('validateNewTask', function () {
  var fn = require('../../app/validation/validate_new_task.js');

  it('fails if newTask missing', function (done) {
    var payload = {};
    fn(payload).catch(function (error) {
      expect(error).toEqual({ newTask: 'MISSING' });
      done();
    });
  });

  it('fails if newTask.* missing', function (done) {
    var payload = { newTask: {} };
    fn(payload).catch(function (error) {
      expect(error).toEqual({ 'newTask.text': 'MISSING' });
      done();
    });
  });

  it('succeeds', function (done) {
    var payload = { newTask: { text: Math.random().toString() } };
    fn(payload).then(function (result) {
      expect(result).toBe(payload);
      done();
    });
  });
});
