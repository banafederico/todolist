describe('presentTask', function () {
  var fn = require('../../app/presentation/present_task.js');

  it('presents', function (done) {
    var payload = {
      newTask: Math.random(),
      task: {
        id: Math.random(),
        text: Math.random(),
      }
    };
    fn(payload).then(function (result) {
      expect(result).toEqual({
        task: {
          id: payload.task.id,
          text: payload.task.text,
        }
      });
      done();
    });
  });
});
