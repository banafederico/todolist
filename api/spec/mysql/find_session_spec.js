var mysqlQuery = require('../../mysql/query.js')
  , emptyDatabase = require('../support/empty_database.js');

describe('findSession', function () {
  var fn = require('../../app/mysql/find_session.js')
    , session;

  beforeEach(function (done) {
    emptyDatabase()
      .then(function () {
        session = {
          token: Math.random(),
          userId: Math.ceil(Math.random() * 1000)
        };
        return mysqlQuery({
          sql: 'INSERT INTO sessions (token, userId) VALUES (?, ?)',
          values: [session.token, session.userId]
        });
      })
      .then(function (result) {
        session.id = result.rows.insertId;
      })
      .then(done);
  });

  it('returns session', function (done) {
    var payload = { session: { token: session.token } };
    fn(payload).then(function (result) {
      expect(result).toBe(payload);
      expect(result.session).toEqual({
        id: session.id,
        token: session.token,
        userId: session.userId,
      });
      done();
    });
  });

  it('cant find session', function (done) {
    var payload = { session: { token: Math.random() } };
    fn(payload).catch(function (error) {
      expect(error).toEqual({ session: 'NOT_FOUND' });
      done();
    });
  });
});
