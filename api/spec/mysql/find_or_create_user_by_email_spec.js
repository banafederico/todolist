var mysqlQuery = require('../../mysql/query.js')
  , emptyDatabase = require('../support/empty_database.js');

describe('findOrCreateUserByEmail', function () {
  var fn = require('../../app/mysql/find_or_create_user_by_email.js')
    , user;

  beforeEach(function (done) {
    emptyDatabase()
      .then(function () {
        user = { email: Math.random().toString() };
        return mysqlQuery({
          sql: 'INSERT INTO users (email) VALUES (?)',
          values: [user.email]
        });
      })
      .then(function (result) {
        user.id = result.rows.insertId;
      })
      .then(done);
  });

  it('finds and adds user to payload', function (done) {
    var payload = { user: { email: user.email } };
    fn(payload).then(function (result) {
      expect(result).toBe(payload);
      expect(result.user).toEqual({
        id: user.id,
        email: user.email
      });
      done();
    });
  });

  it('creates and adds user to payload', function (done) {
    var payload = { user: { email: Math.random().toString() } }
      , result;
    fn(payload)
      .then(function (_result) {
        result = _result;
        expect(result).toBe(payload);
        return mysqlQuery({
          sql: 'SELECT * FROM users WHERE email = ?',
          values: [payload.user.email]
        })
      })
      .then(function (mysqlResult) {
        expect(mysqlResult.rows[0].email).toEqual(payload.user.email);
        expect(result.user).toEqual({
          id: mysqlResult.rows[0].id,
          email: mysqlResult.rows[0].email
        });
        done();
      });
  });

});
