var mysqlQuery = require('../../mysql/query.js')
  , emptyDatabase = require('../support/empty_database.js');

describe('saveNewSession', function () {
  var fn = require('../../app/mysql/save_new_session.js');

  beforeEach(function (done) {
    emptyDatabase().then(done);
  });

  it('saves new session and adds to payload', function (done) {
    var payload = { user: { id: Math.ceil(Math.random() * 100) } }
      , result;
    fn(payload)
      .then(function (_result) {
        result = _result;
        expect(result).toBe(payload);
        return mysqlQuery({ sql: 'SELECT * FROM sessions' });
      })
      .then(function (mysqlResult) {
        expect(mysqlResult.rows[0].userId).toEqual(payload.user.id);
        expect(mysqlResult.rows[0].token).not.toBeUndefined();
        expect(mysqlResult.rows[0].token).not.toBeNull();
        expect(result.session).toEqual({
          id: mysqlResult.rows[0].id,
          token: mysqlResult.rows[0].token
        });
        done();
      });
  });
});
