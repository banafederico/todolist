var mysqlQuery = require('../../mysql/query.js')

module.exports = function saveNewTask(payload) {
  return mysqlQuery({
    sql: 'INSERT INTO tasks (text, userId) VALUES (?, ?)',
    values: [payload.newTask.text, payload.session.userId]
  })
    .then(function (mysqlResult) {
      payload.task = {
        id: mysqlResult.rows.insertId,
        text: payload.newTask.text
      };
      return payload;
    });
};
