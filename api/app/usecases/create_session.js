var findOrCreateUserByEmail = require('../mysql/find_or_create_user_by_email.js')
  , saveNewSession = require('../mysql/save_new_session.js')
  , presentSession = require('../presentation/present_session.js');

module.exports = function createSession(payload) {
  return Promise.resolve(payload)
    .then(findOrCreateUserByEmail)
    .then(saveNewSession)
    .then(presentSession);
};
