function addTask(data) {
  return makeApiRequest({
    api: 'addTask',
    body: {
      session: { token: localStorage.getItem('token') },
      newTask: { text: data.text }
    }
  });
}
