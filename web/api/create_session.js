function createSession(data) {
  return makeApiRequest({
    api: 'createSession',
    body: { user: { email: data.email } }
  });
}
