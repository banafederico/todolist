function newTaskFormTemplate() {
  return `
    <div class="form">
      <form v-on:submit="submitTask">
        <div class="form-field">
          <textarea
              v-model="taskName"
              placeholder="Mi siguiente tarea es...">
          </textarea>
        </div>
        <div class="form-field buttons">
          <button class="button-primary">Continuar</button>
        </div>
      </form>
    </div>
  `;
}
