Vue.component('new-task-form', {
  props: ['new-task-created'],
  data: function () {
    return { taskName: '' };
  },
  methods: {
    submitTask: function (event) {
      var _this = this;
      event.preventDefault();
      addTask({ text: this.taskName }).then(function () {
        _this.newTaskCreated();
      });
    }
  },
  template: newTaskFormTemplate()
});
